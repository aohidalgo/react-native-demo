import React, { Component } from 'react';
import { Text, View, StyleSheet, AsyncStorage } from 'react-native';
import Expo from 'expo';
import { createStackNavigator } from 'react-navigation';

import Login from './components/Login';
import Tab from './components/Main';

const NavigatorTutorial = createStackNavigator(
  {
    Login: { screen: Login, navigationOptions: { header: null } },
    Tab: { screen: Tab, navigationOptions: { header: null } },
  },
  {
    initialRouteName: 'Login',
    portraitOnlyMode: true,
    header: { visible:false },
    headerMode: 'screen'
  }
);

export default class App extends Component {
  render() {
    return(
      <View style={{flex: 1}}>
        <NavigatorTutorial />
      </View>
    );
  }
}