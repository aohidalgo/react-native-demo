import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';

import HomeScreen from './Map';
import SearchScreen from './SearchList';
import FavouritesScreen from './Tickets';
import ChartsScreen from './Chart.js';
import SettingsScreen from './Settings';

export default createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-home' : 'ios-home-outline'}
            size={30}
            color={tintColor}
          />
        ),
        header:null
      }),header:null
    },
    Search: {
      screen: SearchScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-search' : 'ios-search-outline'}
            size={30}
            color={tintColor}
          />
        ),
        header:null
      }),
      
        header:null
    },
    Favourites: {
      screen: FavouritesScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-heart' : 'ios-heart-outline'}
            size={30}
            color={tintColor}
          />
        ),
        header:null
      }),header:null
    },
    Charts: {
      screen: ChartsScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-analytics' : 'ios-analytics-outline'}
            size={28}
            color={tintColor}
          />
        ),header:null
      }),header:null
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ focused, tintColor }) => (
          <Ionicons
            name={focused ? 'ios-settings' : 'ios-settings-outline'}
            size={30}
            color={tintColor}
          />
        ),
        header:null
      }),header:null
    },
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#f4427a',
      inactiveTintColor: '#3e2465',
      style: {
        borderTopWidth: 0,
        zIndex: 100,
        elevation: 100,
        
      },
    },
  }
);