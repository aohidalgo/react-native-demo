import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Picker,
  Button,
  KeyboardAvoidingView,
} from 'react-native';
import { Constants } from 'expo';

export default class SearchInput extends Component {
  static navigationOptions = {
    title: 'Búsqueda',
  };

  state = {
    originA: 'Desde: Heredia',
    originB: 'Hacia: Alajuela',
    destinationType: 'region',
    destination: 'Destino: San Carlos',
  };

  _handleButtonPress = () => {
    this.props.navigation.navigate('Results', { ...this.state });
  };

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View>
          <TextInput
            placeholder={'First origin airport'}
            value={this.state.originA}
            onChangeText={inputVal =>
              this.setState({ originA: inputVal.toUpperCase() })}
            style={{ width: 200, height: 44, padding: 8 }}
          />
        </View>
        <View>
          <TextInput
            placeholder={'Second origin airport'}
            value={this.state.originB}
            onChangeText={inputVal =>
              this.setState({ originB: inputVal.toUpperCase() })}
            style={{ width: 200, height: 44, padding: 8 }}
          />
        </View>
        <View>
          <Picker
            selectedValue={this.state.destinationType}
            onValueChange={itemVal =>
              this.setState({ destinationType: itemVal })}>
            <Picker.Item label="País" value="country" />
            <Picker.Item label="Región" value="region" />
            <Picker.Item label="Ciudad" value="city" />
            <Picker.Item label="Aeropuerto" value="airport" />
            <Picker.Item label="Estado" value="state" />
          </Picker>
          <TextInput
            placeholder={'Destination'}
            value={this.state.destination}
            onChangeText={inputVal => this.setState({ destination: inputVal })}
            style={{ width: 200, height: 44, padding: 8 }}
          />
          <Button title="Search" onPress={this._handleButtonPress} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
});
