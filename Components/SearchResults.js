import React, { Component } from 'react';
import { Text, View, StyleSheet, Alert, Image, ScrollView, Button, Linking } from 'react-native';
import { Constants, WebBrowser } from 'expo';

export default class SearchResults extends Component {
  static navigationOptions = {
    title: 'Resultados de Búsqueda',
  };

  state = {
    tripPairs: [],
    loading: true,
  };

  async componentWillMount() {
    const { params } = this.props.navigation.state;
    const req_body = {
      origin_a: params.originA,
      origin_b: params.originB,
      dst_type: params.destinationType,
      dst: params.destination,
    };

    try {
      const resp = await fetch('http://35.193.117.4:8000/search', {
        method: 'POST',
        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
        body: JSON.stringify(req_body),
      });
      const data = await resp.json();
      this.setState({tripPairs: data, loading: false});
    } catch (e) {
      console.warn('Error while searching for flights', e);
    }
  }
  
  renderLoading() {
    return (
      <Image
          source={require('../loading.gif')}
          style={{ width: 150, height: 150 }}
        />
    );
  }
  
  renderResults() {
    if (this.state.tripPairs.length === 0) {
      return <Text>No results found...</Text>;
    }
    const rows = this.state.tripPairs.map((pair, i) => {
      return (
        <View style={{marginVertical: 20}} key={i}>
          <Text style={{fontSize: 18}}>
            ${Math.round(pair.total_cost)} for {pair.trip_a.copy.description}, {pair.trip_a.copy.label}
          </Text>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
            <Button
              title={'$' + Math.round(pair.trip_a.bestPrice).toString() + ' from ' + pair.trip_a.origin}
              onPress={async () => {
                try {
                  const result = await WebBrowser.openBrowserAsync(pair.trip_a_link);
                  console.log(result.type);
                } catch (e) {
                  console.warn(e);
                }
              }}/>
            <Button
              title={'$' + Math.round(pair.trip_b.bestPrice).toString() + ' from ' + pair.trip_b.origin}
              onPress={async () => {
                try {
                  const result = await WebBrowser.openBrowserAsync(pair.trip_b_link);
                  console.log(result.type);
                } catch (e) {
                  console.warn(e);
                }
              }}/>
          </View>
        </View>
      );
    });
    return (
      <ScrollView>
        {rows}
      </ScrollView>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? this.renderLoading() : this.renderResults()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
});
