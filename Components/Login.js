import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, Alert, Image, TouchableOpacity } from 'react-native';
import { Constants, Google } from 'expo';
import * as firebase from 'firebase'; 


var firebaseConfig = {
  apiKey: 'AIzaSyDeiWJyPBzim_yMDUo-csLkwfblVt6dAWE',
  authDomain: 'alset-snack.firebaseapp.com',
  databaseURL: 'https://alset-snack.firebaseio.com',
  projectId: 'alset-snack',
  storageBucket: 'alset-snack.appspot.com',
  messagingSenderId: '873097287154',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

function storeHighScore(userId, score) {
  firebase
    .database()
    .ref('users/' + userId)
    .set({
      highscore: score,
    });
}

function setupHighscoreListener(userId) {
  firebase
    .database()
    .ref('users/' + userId)
    .on('value', snapshot => {
      const highscore = snapshot.val().highscore;
      console.log('New high score: ' + highscore);
    });
}

firebase.auth().onAuthStateChanged(user => {
  if (user != null) {
    console.log('We are authenticated now!');
  } else {
    console.log('We did not authenticate.');
  }

});

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };
  _handleGoogleLogin = async () => {
    try {
      const { type, user, idToken, accessToken } = await Google.logInAsync({
        androidStandaloneAppClientId: '<ANDROID_CLIENT_ID>',
        iosStandaloneAppClientId: '<IOS_CLIENT_ID>',
        androidClientId:
          '603386649315-9rbv8vmv2vvftetfbvlrbufcps1fajqf.apps.googleusercontent.com',
        iosClientId:
          '603386649315-vp4revvrcgrcjme51ebuhbkbspl048l9.apps.googleusercontent.com',
        scopes: ['profile', 'email'],
      });

      switch (type) {
        case 'success': {
          this.props.navigation.navigate('Tab');
          Alert.alert('Logged in!', `Hola ${user.name}!`);
          console.log(user);
          console.log(type);
          console.log(idToken);
          console.log(accessToken);

          const credential = firebase.auth.GoogleAuthProvider.credential(
            idToken,
            accessToken
          );

          firebase
            .auth()
            .signInWithCredential(credential)
            .catch(error => {
              // Handle Errors here.
              console.log('Error authenticating with Google');
              console.log(error);
              console.log(error.message);
            });

          break;
        }
        case 'cancel': {
          Alert.alert('Cancelled!', 'Login was cancelled!');
          break;
        }
        default: {
          Alert.alert('Oops!', 'Login failed!');
        }
      }
    } catch (e) {
      Alert.alert('Oops!', 'Login failed!');
    }
  };

  render() {
    return (
      <View style={styles.container2}>
        <Image source={require('../assets/splash.png')} style={styles.imageContainer}>
        </Image>
        <View style={styles.overlay}> 
          <View style={styles.buttonContainer}> 
          <Image
            style={styles.logoGoogle}
            source={require('../assets/G.png')}
          />
          <TouchableOpacity style={styles.loginButton}
            title="Ingresar con Google"
            onPress={this._handleGoogleLogin}>
            <Text style={styles.googleText}>Google Login</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.paragraph}>
          ¡Ingresa a Mapplee para un camino menos cansado!
        </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  paragraph: {
    margin: 25,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#f4427a',
  },
  loginButton:{
    backgroundColor: 'white',
    height: 40,
    justifyContent: 'center'
  },
  logoGoogle:{
    width: 20,
    height: 20,
    right: 10
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 100,
    flexDirection: 'row',
    width: 250,
    marginTop: 50,
    alignSelf: 'center',
  },
  googleText:{
    color: '#80bf38',
    fontWeight: '600',
    fontSize: 15
  },
  container2: {
    flex: 1,
    width: null,
    height: null,
    
  },
  imageContainer: {
    flex: 1,
    width: null,
    height: null,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    top: 250
  }
});
