import SearchInput from './SearchInput';
import SearchResults from './SearchResults';

import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

export default StackNavigator({
  Search: {screen: SearchInput},
  Results: {screen: SearchResults},
});