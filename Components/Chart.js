import React, { Component } from 'react';
import { WebView } from 'react-native';
import { Constants } from 'expo';

export default class App extends Component {
  render() {
    return (
      <WebView
        source={{ uri: "https://www.youtube.com/watch?v=En1lq8eBztk" }}
        style={{ marginTop: 20 }}
      />
    );
  }
}
