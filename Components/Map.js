import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Constants, MapView, PROVIDER_GOOGLE } from 'expo';

export default class App extends Component {
static navigationOptions = {
    header: null,
  };
  state = {
    mapRegion: {
      latitude: 10.3209204,
      longitude: -84.4264859,
      latitudeDelta: 0.009,
      longitudeDelta: 0.009,
    },
  };

  _handleMapRegionChange = mapRegion => {
    this.setState({ mapRegion });
  };

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{ alignSelf: 'stretch', height: '100%' }}
          region={this.state.mapRegion}
          onRegionChange={this._handleMapRegionChange}
          showsUserLocation>

          <MapView.Marker
            draggable
            coordinate={{
              latitude: 10.3186541,
              longitude: -84.4288211,
            }}
            title="UTN"
          />

          <MapView.Marker
            draggable
            coordinate={{
              latitude: 10.3194901,
              longitude: -84.4253541,
            }}
            title="Parada Autobus"
          />

          <MapView.Marker
            draggable
            coordinate={{
              latitude: 10.322274,
              longitude: -84.4242465,
            }}
            title="San Martín"
          />

          <MapView.Marker
            draggable
            coordinate={{
              latitude: 10.3225203,
              longitude: -84.4300577,
            }}
            title="Catedral"
          />

          <MapView.Marker
            draggable
            coordinate={{
              latitude: 10.3209204,
              longitude: -84.4264859,
            }}
            title="U.San José"
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#085274',
  },
});