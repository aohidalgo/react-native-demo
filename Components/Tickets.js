import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  View,
  StyleSheet,
  Image,
  Button,
  Alert,
} from 'react-native';
import { Constants, LinearGradient } from 'expo';



export default class App extends Component {
  _handleButtonPress = () => {
    Alert.alert('Button pressed!', 'You did it!');
  };

  render() {
    return (
      <View style={styles.container}>

        <LinearGradient
          colors={['#ff5c5c', '#3e2465', '#085274']}
          style={{
            height: 70,
            width: 375,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 2,
          }}>
          <Text style={styles.country}> MIS TIQUETES </Text>
        </LinearGradient>
        <ScrollView style={styles.card}>
          <Image
            source={{
              uri: 'http://www.wagnerslanes.com/assets/wTicket.png',
            }}
            style={{ height: 220, width: 350, marginTop: 10, borderRadius: 10 }}
          />
          <View style={styles.textbox}>
            <Text style={styles.text2}>
              Bus
            </Text>
            <Text style={styles.text}>
              Tiquete de Bus

            </Text>
            <Text style={styles.text1}>
              {' '}25 November, Yas Marina Circuit, Abu Dhabi
            </Text>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'http://www.dreamdays.ae/dreamdays_uploads/supplierImages/60/497tour/images/dreamdays-greenplanet1.jpg',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Viaje</Text>
              <Text style={styles.text}>Zoológico De la Marina</Text>
              <Text style={styles.text1}>
                25 November, Abu Dhabi Forest, Abu Dhabi
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMVFRUXFRcXFRcVFxUYGBcVFRcXFxUVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lHR8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgMEAAIHAQj/xAA4EAABAwMDAgQEBQMEAgMAAAABAAIRAwQhBRIxQVEGImFxE4GRoQcyscHRFOHwQlJy8RWCIyRi/8QAGgEAAwEBAQEAAAAAAAAAAAAAAgMEAQUABv/EACYRAAICAgMAAgICAwEAAAAAAAABAhEDIQQSMRNBIlEyYQVCgRT/2gAMAwEAAhEDEQA/AOb29SMovaXR5QGk5GrGljK5eeK6gokq1XOUlCzJVy2t+pReytxC508taQ2MbB9pp2Qi1Ohs9E16VpbWNDnCXHv0/utddph1JxPLRIPspsnaY549C4LotVOpfGVLQYHZKndp+JCD8cfommDrW8Jqc9EyWl1AStd0NuevRZa6m8YVPxKcbQKl1HT+swht5dlUm3RI9VuKZOSopqMWH2sHXd3zKD17vsUXv7SUu39AtXV4coPwmyJk1K6zyi1tXSrTJCK2teFblYpRGWhcFFbCpMhAtKl5R+iyOFMstSoaoDXoZgAD3TZSdhJ3hqr5od8k0By62GXaNjEqLbnJI8ZaoQQwSPNP8JscSkX8QSAaR6yQfZNfhkvCTTPETwADlHaGuk9R80i2rw7jKvMqkd8CeFLLJJeHkPVLU56heuuJSfQuzhwHPHz4Vx+ouBiMoI8hv0Jm3iW68kJMrXbQCSeEb1Lc8zBPyKX7zSwRklQ8qKySti7PKepbuuVK+6cRkoTS05zHendX308Lk5ccYSpBxk6CWm3DWkEfMI9VuaLm5cB74SWK23qrbLoOasjKadpWMXhHfaqyhctfSdvBEPAmMcEFOtj4goVGzvAxwefouU3p8y3o1OxXahkl8ehd7HSpcuk7XGJMeyxLFO1eQDuP3WLmPFsOxDtRJTRZswlq0hpRelcngLt5o9gEGHVgDARTRLhvxGBxgbhKVHVyt2X8KWfH1oOMqZ2N9xCWPFGsNaw0wfM/EdmnklJFTxHWiBUeB7lUKNUvdJJJJ+aXHjVuQ2ea1SHnRRuhMJaAl7QakBHS+Vw+Q28p5PQG1WkCCVDoOguqnc7DQee/oFavXgnb6pq0VoFJscR95MroY24woCMLeyGloFKODPeVpc6XsxyOiYKEKnq1QYHuo+Ri/Gx3VUKN5QiZSdq7huTlrNxgpXoaV8cnzbTOC78pP+0u/wBJ91Z/j10/KRNki5OkLznBTWxymK58HVdofs2wYqRkATG8enUxhPXh/wAE0TTbvw9og7eHCcSPUQur8inqO2ZHjy+xN0E9O/UroGkeHnES+IPY5HsibdItKABDGyOJ6KR2rsIwR8kOPB+VzHxxUj1mlsp5B4XlfWqdP8zhOMJP1/xltljPN0Bn6pftqjqg3zJkyDiOft/nRUrsv46Q5Y41s6lT1ymSB34Pp/hVbUrO1uCC85EgZxnn9AkanfREmMR+iiraq7eA0+Xk/XhF3k/s98Uf0dDoaNbNb5YgYlSjSqJB2lueUiP8Q7RBI549D6e6jutfhstJk5xj+5XnK14D8KH0aTt2how0Y9YED91Xp6Sd0ub9OB6Ja0Tx1vcGOOQIiJKd7PVA6P3QQ6qW0eyYnRE22AxCF63o4e3c0DcPv6JodRDvdUrhpbgqmeOM40yRxOabehCrup9Ci/iHayoXD/V+qD1HyvleVBrJRkQHqbdjvdRPu9o9FdumAgyM9FWuLJ+zdsIHUxhWYlUdnr2ALurJlWrKp1VGu3zELe2cQFc9xM+xzo1QWg+i9Sd/5QjEnCxI+Bje6BtFg6q5RPZVWhT7l0JE9mteoQqwMlSuWgYt1QcUbtpg4hENNoQ6D0ValTVylV83tAU+W2qQzQ36ZSkgN5JgJtt9MY0eeXH3gfRJvhO5/wDsNBPIdHvCdn1Vy48epWx2OmgHrmlNDTUpyC3zFsyCBzHYoZpXiU0sfmaekwR7Jh1GsG0qjjwGO+4gBcxFIyndYv0DJ+L0dJb4uZHlaZPc/wAKB2ol+cklJVs2SG+qY7d0CBwp82M8pN+keoklEPCFEGo5uRuGTEg+hCqbxPnmPSJ+6PeF6A+I17adRvUOJ3Nc35ACQtx7XU3Gn2sb6WymwCpAwRHcdgOqDatrlOkyGEgAegwOhlJWseI6jbqs8mBuLGDnytxJ++EuavrjnkCecn2hdyGJdUPumMmpeI21Ww2o0OmCId+sR91pqmuilQ203gPcIwQYPWSMf5ykrT68nyjAG4kYJEdT0GVTbdPr1w1pLWt6NkTnkxn6lMWNIyWRsPaZSk7qhBPMtJBPrnlFbjUYGI/T/Cg99qjKbYmXDHEgH1KE2+qbjnM8/wAImjyaL95qxn7fVQDV3QQDz+yp3FOZcFBZ0J9v7oepvdhi2uS7J/yEUp1i4RMT1OEDa9rAp7K/DjtJHpK91Nsmq0/hu3NqNkHqU0+GfFjohwJjr+YfXKV9Stjthw54Pf7goDodwaVYgEZ9TMj+0rXBNA9qZ3TTfFoLsuiOhj7pysrttZsmMrglasXCRg8gj/ITH4W8UOaQyoZj1jHcHql/lDa8DcY5FX2O3ifwu6rDmSY59f4SFfWFaiQ17DJ4wee0xn5LrGl6k17RDp+eV7qlFjm5DZ7ls/VTZuPGf5xf/CWWKnTOceGfDz61XfUEMb0PUp3q6K0tLYEEZnqiOmW4A6fIBXi1WYePGMKYmj578b6C61rCB5HflPb0KoU7Tyrsfj/SBWoOxkZHulHw9YNZSG8AvI805j0Ch5MHDUT32c0rWWSsT5eeHZe4tIAnErFOuSzxzoVFs16pU3q0zldWSF0XBRkLRzdqs0zhQV1PG7DNBcE4AhS0iQoRhSNMo5eHgha13BzS0wQQQR3CdLTxUNo30iXdS0iD6weEl2HIR23ogwFFlY2Da8L2o6g+4xG1vIaOp9T1QoUYRQUw0Y6KNrcT1UcnWwmrB4ZtcDCMUVCymHKx/TwMEpM8t6NSo0r1SAr/AIS1h1J5YXEU3dOgceo7ILcMduAOVO2nkTgSJP8A0ijJrZ5Np2BvG9FzLl44aZc3tByD90pmuXOJ9Pp/gXUPxKt21LanUblw8riONpyJPyP3XL6VHsvoONk740xuRb0SurbLcnjccfIQP89VBpdfYyGg7nnpkn5rTV2mAwZ2CXehJwF5pVMmI56Kj6F/ZM1pMF1R4ByA1shsdHTEn0HrlbstIy3rBA9DlGre1Ls1Aw9/KCSekmOUf07wuT5nAZMgDgLzejVGmA7W2MARmMheG0IBj14T43SGAeq0q6KCPLHt3WBnLbmg4khVKds9riIB24dudtAP+0ZGffCeb7SnU3F0ckesQg2o6Kwuc/zkPcXHAdDiZceh59V6L/ZkotrXpJomqNcPhvPlOIOS1w7FLutt+DXlvG4Gfr/JVurbMb+Rrg6Z3O6x0AGAEL1S43mT2Wr0Gd1sahXmmxw747z0z7Ee60F5BBbxPH7eh5+yGWD91v7EfVvBXlRxI9R+3BWUFY82viF1OCCePr3B/ldU8Haobihudkgx8oBC+e7K4LnbO5x7r6B8FWbqFqxrh5iJd7np8kpRSloLJK47GBgUxUTSq91XIVN0iMr6zBYR6LnVSptrPb0mR7FN+o3Diwj6rl91rO25c05AwCOhk49VByZKSMY1lwWIKNVZ6/RYuQes5LRKs06iqU8KYOX0TQphOjWWtZy0tbZzhIBK3qUjwVK2kzUQhyuUGqm2kZV6jTQZJa0Gi9ZjKNW1cTlBbdh6BWxU7jKim7YadBWvXkRwFvTktQptweEUtn4UmbS0GnZet6EgD0VgUo4KjtqwRiwsg/zHj91zm5dhyQIvBABhUfizlON5pstwPsl/UdO2tkCFXimlpgZbjsJ6PSpVqbqD/MKgM/8ArgRPT9T7JN1HwnUoVC0bYnyPdAbkjbPcjOB2RnTfENW3a5rdvTaC0YyJA7YkqGnWqXVRrrh7g3/TAwPWOjZGXenpC6eGbgtC3zI9VFK2b+NvCtG3sKfwhLpmo8xue88vd/HQJC0y3gSutePqQbZU28g8EGQB0gnp7rnNO3DBniMARK6OFtx2US3TDHh2zDpqPJLWdB1P6JmoazRZTNZ52tBhoOc+gHJKV7qv8K3FIGC7zOHXOQD9kka2+rAa1xLQSYngnkx8lRHbBfh1Q+M6D3QHAe7DH6q5c6tS2TubMSCzrHp0XBhUdMA590Z0MVThxc0d+vHAHVHLrWkZG2dWbdMrtP6GM+0JYvCWuLcxMf8Aa10KvsOHSP8A9QvfEFYte1+3Bwf2SRoOvaGJj6pR1GjmQnDeHjGB0BSvdtIqbXH6r0WDMn0AxNM8P/L/AMhmPdWK7SCR1T5+HXhKnd0azKgLQYLXiJY8flc2es/VXKH4eVHXDW1o2jl7Pyv9v9pPYpfyr2g1CtWCvws8O/GrfHeP/jZxPBd0+mV2y3cMhD6Vgy3p/DY0MGIEfdZ8badyyMnF9pCsrT0gvKr3cQqdPU2nqq91eh2BwlZ+djjDTExi7KV+PI4e645rVHbcE9CV2C8I2n2XL/F1HaS5c3DyO+TqwsipEbKwgLEFbdiF6n/+UTaKlbTt1U9BGfdaXGllsRkfcI61oL3e/wCy2LFa8jCcVRHZ0oEdlJf2stmMj9FLaNU13WAbt6n9FHOTbPNaF/4MKW3py4BTvAlSWgAcPosc2kCgjTpGBAVj+iLhxwrVlS4Rb4YDVBPK7HddCi6gQVYoPjCv3dFUSFjl2QvxhPTHy5OukgRC5/avIKcdGuZgg56qTKqdlGOQxhqEazQlpEc8Io2thQXNPcEh5Leg5q0c6rWbmvJInt2n17qfSWVBVFQmQ0yQ44OIiPsmmtZAYhRVbBm3qPYD+V2sE+0dnz/NU4PtB+EniSm2tYObTaA+n59oJcA0/wCpmMjn2XMNMY4sG7G10Z5IJ6f3XY9AoBjQATwdwccgO7A4g+i5h4isP6W7q8hk7gD1zIAXTwZNUdfjTlkxJy9KHiB8PPA6c5wli9buKN+IblrnBzeCAfrlLV1fBpiFVGx9pFQ23mhFbB2z3QX+tMzAVu31Bs5EfojkmZGcRio3hkEY75TDRqh9LzEcxE/RJdG5A7K/cas1lPa0S49B0nqUumHaL1yNklpBHYpcFf4tUEiOmFPRrl1M7jk/p/KOeB9CFzdU2HDQ4FxHUenqscuqZiXZnZfw0000rLsXTHPy6KPUyWbiXVCSTkubj/j1HeEX8SXbKFJtLa7aRtG0B3HSFzS91MMJiYM44PofeUrq1pfXovLkt3+xl/8AM1AY+JuHr/dZV1zdiZ9lzqvq9QnJV7Sbgk8pXJi+miZT2O39aAO6jN+SeyH0yvHlcGWJ2M7heveDblIHjGvIKY69x5Uk+Ka3QKrh4KyJnpStC9/ULFUlYvoOiEjRZXYG4OMTke60utTEQ3JPXsh127aPUqlTclfHqzWwvbXbm4BwpPj9ScofvXjXEmEnp9nrCLXyVcohVbW3xyrNId0mUEzwbsrwiO3qiH9Z3KV31SFXq6i4YlIfF7MYp6Gi4uRyqTH7il//AMiSiekuLihlxuqsG7YxWlMcQi1oza4Qq1jSRJrMLh8mW6KIoK2lQnlE6aEWNQdUV3YU/HfWQ5bR5Xoz0Q+rTA5Ix/gV/wCIg+pu5A6/su9gaa0cnnQ+yzY1i1wxuaXdDL2d3R/tJ+SDfijY7ixwH5m5Psrem2jS9rnPeC0D8uMnMg9OSrv4k1Gssdx5a5u39/tK6OB+jv8AHxmofkcYZpdSsx2xrnFjhhsEmSAAAqWtaHUa6HgB8AQMjbAMg/P9UxafdO8rrYxv8rzO0DuCeg6T6r2pdtrVYqFm4ABsYgAflA6n+66UKejo4oKT2xCq6Q8ECOVJT0OoTAauhP08YgZ9lBdVGUR5iN5w1o5zjcewCZJUipcSHsnoTBYuYDTIIJzIzgjgEfP6KetasptDRBJye/8A0rl89zD+X37SPT15VF9QEyVPbZNLrHSN6IA+S6n+ENoDV3RwC4/t+q5I2p5pPddx/B1gbQq1Y+ft0S5raX9gxf4t/wBG/jm8e152t2hziN4MSMYA3feP1XONTrEuyZTZ4s1Fz3eUFrZdIJMOMyHbTxyeEqOtnOcthGodmR5n+dFAUyUX0+kRCt2em9IRhmmABS5czeqAUDS3crW3sqz6Bb7IlplDc4Sonjk3aRq9oq1tJc9szt7T/C594m059N3mIIPBC7JVpYSP410+WD/krMGOaktBzjFRs5gWrEXOlFYup8U/0T90DahnrKjAXrCt2hJboJnrQUU06j+iHUnZRG0cQZSMstGIJU2QpK3C2AESoKpnhSxnYbRVrVUOqukowKMhU6tlCohNGJMqW7JTFpNOEIbThE7B8DKVnfZBxHG0uIAV4XGEsMu+AjVs6BnlcPJxnP0bbL9GuZRehdEjuglJ4dz9sLdtzBhST4klLQcZUHDVMKjcZK1o3AIyVLQILw2Rkrp8SLjpg5YKYwaTZNDQ9zYPviP34XPPxg1cuY2mDiZ+i6Bq918KltBjC414/qy9rZBOT16rvQxqKoOKpaBHhnV20GvDvzOPlHpyTPTt81T1Sqx5J2lvWfX0/lCLhpBXhuiQAcj1Tkt2Z2fhY/qKvAr1A09N7v5W9IhmTLjyf59VXZfMiDTntlT0nBxwI7DsjZ5TkWLq+BGMGMA9f7ocasZOSiZ0V0bjlDXUwCUFJGtt7ZpSaSZXcvwY1AbXUi4d4XFKTk0eELw0q7HboyP1ylZL1JfQyCTTX7O0+MfD9KoN5EOwN27bGe/BgF3PdIbbMNqOZ0BxkHHuF1+m1teiJghw6eoXO9b0c0a5jeWngvJcTHr2VGKCbf6ZFm8/tGthbAK69gAUdqzCsGnKc+NFsR8joHVWCCpdPqhpCsOtFqLJNWHHXgtyldhNzgQlrxAA4hvZFhRcBAJCrvsUWPHGLs3JNyjQq/0I7LEym0CxVdok/VnCKZVqmVX2KVgXFyRL0bEpg0uMJeIRrQ3T8lLlj+IUVsa6toHNxyqdKwmR1Ra1rjbPRR0HCZHVcfJNwHuKB9HT8xyiZ8MuLZLg2ekSUf8ADli1ztzhOce6Zqls0jgfRKhyJvdhxxL7OQ6lo76WTBb3H79lUgYhdM1awwREtIhc2uW7SQq8WZz0xc4dSe0qiRKYt8wRx0SaK0FXbTU3MxyOx/ZE8LfgCkkNlFqr3NYh/wBFVstQc/gRPzKJM04kSefVejrTNbvw0o3SNeGyH1gZmMpbvrdzRx9EZ/D0bnVHnO3AVWKCclRqtaCPi++DTkjHSR91yTxXeCpV3DP6Ji/EG/LqrgMx6rn9aoYz0V62xvioytlQGmFLTdIWhajBKj6WcKxakgyvQDKka1Y2eobtIrh7dpjsqGq6Ntk4hUtLuNjh26pnrD4lPrwhG1oSabYcitGpEEdFQuaBDyP1Vmk4gcIWbHR2/wADa5Nu0OJkRH8FNms5ph4nHY9DyuO+DLzawj1kLr+i3Iq0tpg4iCthrQGVf7C65jJG0R35P6qanRU1eiQ4t+CGkHlu7P7KWm2OQq4ydHPktkQoL34CtiFq5yKzKRVNBaPoqw+soXVF62Zooup5WLZ78rE62L0cDp0lMaa0a5bipIXMzu3ooiV61OFJY1i0yFlQyFJY2b3kNa0uceABJQa67PbsMW+oO4nCu0L4kgKm7Q7ljZdScB7TH0UFrbOc8AclQZcUJJjVJnWPCdQPZA5Bn5JqFNpSV4NpmnuaXZIBH7psbXPWFz8UIwTtFauih4gZtZ+nuuW6xYOEuGf1XVtZM03T2x9UkagwRK9jko5bXgORWhHaZ6K5Z2Ln8Aq/a6cHVtvTn9030NPAAxC6MsyXhLGHYE6LYlpkgpopUMcKK1owe46yiQGISJflsohGgJqFHGQpfBdFrGVYndOZ7dFbvWeUrPD9sWUqr3HBkN56J/F/mbJHNfFwmq+O6T7ikm7XSPiOnul6qJXSj4awaymVbp0MLYMUtMSYXmwoxRSrBeU2Side0BHZQ0LeCsQTib29o7BhOOmUPLB7LXw7twxw+qbmWjAMAI4QvZknSOZ69pDmuLgPsUHaSus39AFpaY+YXNtSohjyAV6cKBi7DPhav05Pbv7eq6l4ZvQ1wE4PdcY0+sWkELonhDU/iODXRPccpb0E1Y+6+wghwJg9BMT3Q+lURTVnnY0fdAqlWFTB6s52VUy4+uqNxfAdVQvL2OqVNR1gylSzpOhEpDa/UR3Wzb0FI1LUieqK2V9OEzHk7C+wbfcZWIYahWKwyzj7K6jqVs4VVlRe7lz3HZfQRt6sp+/Dl7d7wQNxaNp6wD5h9wfkub0KkJi0TUjTc1zTDmmR/Ck5KajoKGmdjFNL2racynWZUAA3y09t3M/NEtH1mncNlpAcPzM6j+R6oR4q1JhcymCCW5dHQnAHuuZJuimdVZfoEtIIwRwjTdZbtO5pGOkQlrStQDhDuR1V2u7c2ADnuIUPy1Zqlonu9aFUbYLR3PX+EHv67QOZ9At69liRhCbmm5vKLCnJ3YEpOjNNrkVdxHX7Jzp1AQkD4kFXLbXnMEK54nLaFRl19Hig4SvLmvDsFJ7/ABC4jGFZtdTJHP1W9JJeDI5ExkfVJVnXLkUrQeoQixeahAB6q144eBTa0nge6q4cGrbCk7o5VqFwC47uSZ/zshxqBe6q7zFD2u7roJaPNl17lqypBVZ1ZeMcvUeTDNs6eVs8AKrQfClLZWDUxj8Ot3PB7fsnMvPZAPD1l8NgJ5MT/CM/EHAVGNUgJSs01ESw5Ixyua6n+cg8roF5kO9lz7VKe15Q5TIm1kmzwpc/DqtPqkqlVTL4YqAugkfPH9lPJWhiZ2DXb4BrBnInCWLjUGt6T7kx9B/Ks+K79rLei6JJaQD249Vz661ieqD5V/E5nIbUwtqWoSSUr3dzJUV1fl3VUqj8IFEn9J/6qEa0a66pRqVFbtL4sVOPR5wHo3oWJMdq5lYrPlA6MVIW0rxYpjoG7HK9a1IKxYskk0CFaV3HUg+iktLnzLFimWOLT0bbOheFqI2B/UmPom6lbiMrFi+f5MEshVj8KGoWu0GOEtam8BqxYncKEewvK6ANUcqjcOWLF9Biiv0TWa03K1bvKxYmyhH9AXscvBtYGq3HE5VDx9qJ3OAJmVixKSS8K8TtHMrtx3EqlUqdF6sRoJkJKv6fBICxYtPDHQ0iYIRa101ogkdfusWJqgjzkwy+5GCZjoFsbuBiAsWIzGUbq7wSUq39+x4iM9P+1ixImw14BWVMo3olXa8HPKxYlM1HRvFOoOZYUdjy15MCJyDzlcvvq7gSHAbjkkyT9ZiV4sUqVzZLn/kU21CvXPWLFUkiZ+kD1E58LFiNBEJesWLEZp//2Q==',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Pet Shop</Text>
              <Text style={styles.text}>Cutest Puppy Show</Text>
              <Text style={styles.text1}>
                28 November, The 02, London
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'http://stagedoor.fm/wp-content/uploads/2018/05/Jorja-Smith-February-3rd-Lost-Found.jpg',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Music</Text>
              <Text style={styles.text}>Jorja Smith</Text>
              <Text style={styles.text1}>
                28 November, Wembley Stadium, London
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'https://i.ytimg.com/vi/rNUYdgIyaPM/maxresdefault.jpg',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Film</Text>
              <Text style={styles.text}>I Am Not Your Negro</Text>
              <Text style={styles.text1}>
                3 February, Roger's Center, Toronto
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'https://thumbs.dreamstime.com/b/bus-train-ticket-vector-10013150.jpg',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Cute</Text>
              <Text style={styles.text}>Puppies and Ducklings</Text>
              <Text style={styles.text1}>
                6 February, Roger's Center, Toronto
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'https://www.katyperry.com/files/2017/03/TOUR_MASTER_DESKTOP.jpg',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}>Music</Text>
              <Text style={styles.text}>Katy Perry</Text>
              <Text style={styles.text1}>
                14 June, The O2 Arena, London
              </Text>
            </View>
          </View>
          <View style={styles.card2}>

            <Image
              source={{
                uri: 'http://everydayfeminism.com/wp-content/uploads/2015/01/Screen-shot-2015-01-07-at-12.42.20-PM.png',
              }}
              style={{
                height: 220,
                width: 350,
                marginTop: 10,
                borderRadius: 10,
              }}
            />
            <View style={styles.textbox}>
              <Text style={styles.text2}> Education</Text>
              <Text style={styles.text}>Intersectional Feminism</Text>
              <Text style={styles.text1}>
                15 June, United Nations, New York
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 10,

    backgroundColor: '#f6f7f9',
  },
  textbox: {
    paddingLeft: 8,
    height: 90,
    width: 350,
    backgroundColor: '#ffffff',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,

    justifyContent: 'center',
  },
  card: {
    alignItems: 'center',
  },
  text: {
    color: '#353c42',
    fontWeight: 'bold',
    fontSize: 25,
  },
  text1: {
    fontFamily: 'string',
  },
  card2: {
    alignItems: 'center',
  },
  country:{
    textAlign:'center',
    top: 20,
    color: '#ffffff',
    fontWeight: '800',
    fontSize: 24
  }
});
