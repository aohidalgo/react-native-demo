import React, { Component } from 'react';
import { View } from 'react-native';
import { Constants } from 'expo';
import Main from './Main';
export default class App extends Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <View style={{flex:1}}>
        <Main/>
      </View>
    );
  }
}
